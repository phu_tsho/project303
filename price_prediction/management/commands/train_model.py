from django.core.management.base import BaseCommand
from PriceApp.models import PricePredictionModel
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import joblib
from sklearn.preprocessing import LabelEncoder, StandardScaler
import pandas as pd

class Command(BaseCommand):
    help = 'Train the machine learning model'

    def handle(self, *args, **options):
        # Retrieve data from the database
        pricedata = PricePredictionModel.objects.all()

        #create a data from the database
        data = pd.DataFrame(list(pricedata.values()))

        # Preprocess the data
        # Handle missing value
        data = data.dropna()

        #Encode categorical variables
        label_encoder = LabelEncoder()
        data['car_name'] = label_encoder.fit_transform(data['car_name'])
        data['car_model'] = label_encoder.fit_transform(data['car_model'])
        data['gear_box'] = label_encoder.fit_transform(data['gear_box'])
        data['fuel_type'] = label_encoder.fit_transform(data['fuel_type'])

        # Scale numerical features
        scaler = StandardScaler()
        numerical_features = ['distance']
        data[numerical_features] = scaler.fit_transform(data[numerical_features])

        # Split the data into training and testing sets
        # Split the data into input features and target variable
        X = data.drop('Price', axis=1)
        y = data['Price']

        # Split the data into training and testing sets
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

        # Train your machine learning model
        model = LinearRegression()
        model.fit(X_train, y_train)

        # Save the trained model
        joblib.dump(model, 'trained_model.joblib')
