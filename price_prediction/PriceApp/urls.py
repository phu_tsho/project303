from django.urls import path
from . import views

urlpatterns = [
    path('',views.Home,name='Home'),
    path('prediction/',views.Prediction,name='prediction'),
    path('prediction/price/',views.Price,name='price'),
    path('data/',views.Data,name='data' ),
    path('suggestions/',views.suggestions,name='suggestions' ),
    path('export-data/', views.export_data, name='export_data'),
  
]
