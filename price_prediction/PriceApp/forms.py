from django import forms
from .models import MyModels
from django.forms import ModelForm, widgets

class MyForm(forms.ModelForm):
    class Meta:
        model = MyModels
        fields = ['car_brand','car_name','car_model','car_gear','car_fuel','car_kilometer']

