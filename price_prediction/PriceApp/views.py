from django.shortcuts import render, redirect
import joblib
from .forms import MyForm
from .models import MyModels
import openpyxl
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.core.cache import cache
import csv
import sklearn
from sklearn import *

print("sklaern version",sklearn.__version__)




def Home(request):
    return render(request, 'index.html')

def Prediction(request):
    model = joblib.load('modell.joblib')
    if request.method == 'POST':
        
        carBrand = request.POST.get('car_brand')
        carName = request.POST.get('car_name')
        carModel = request.POST.get('car_model')
        carGear = request.POST.get('car_gear')
        carFuel = request.POST.get('car_fuel')
        carKm = request.POST.get('car_kilometer')

        # predictmodel = MyModels(
        #     car_name=carName,
        #     car_model=carModel,
        #     gear_box=carGear,
        #     fuel_type=carFuel,
        #     distance=carKm
        # )

        # predictmodel.save()

        predictions = model.predict([[carBrand,carName,carModel,carFuel,carKm,carGear]])

        pre_value = predictions[0]
        obj = MyModels(car_brand = carBrand,car_name = carName, car_model = carModel, car_gear = carGear, car_fuel = carFuel, car_kilometer = carKm, result = pre_value)
        obj.save()
        cache.set('prediction_global', predictions)

        
        return render(request, 'price.html', {'result':pre_value})
        
    return render(request, 'prediction.html')

def Price(request):
    return render(request, 'price.html')



def read_csv_file(file_path, price_range):
    data = []
    with open(file_path, 'r') as csvfile:
        reader = csv.reader(csvfile)
        next(reader) 
        for row in reader:
            try:
                price = float(row[3])  # Assuming price is in the third column (index 2)
                if price >= price_range[0] and price <= price_range[1]:
                    data.append(row)
            except ValueError:
                continue  # Skip the row if the conversion fails
    return data

def suggestions(request):
    prediction = cache.get('prediction_global')
    if prediction is None:
        # Handle the case when prediction is None
        # You can set a default value or raise an error
        # For example:
        # raise ValueError("Prediction not found in cache.")
        my_prediction = 0
    else:
        my_prediction = prediction[0]

    lower_prediction = my_prediction - 20000
    higher_prediction = my_prediction + 20000
    price_range = (lower_prediction, higher_prediction)

    # print("This is the price ",my_prediction)
    file_path = r'D:\Prj303\project\preusedcarprediction\price_prediction\datasets\Final_dataset.csv'
    data = read_csv_file(file_path, price_range)
    data = data[:10]
    # print(data)
    
    return render(request, 'suggestions.html', {'data': data, 'price': my_prediction})


def Data(request):
    data = MyModels.objects.all()
    paginator = Paginator(data, 10)  # Show 10 items per page

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, "data.html", {'page_obj': page_obj,'data':data})


def export_data(request):
    # Retrieve all MyModels objects
    queryset = MyModels.objects.all()

    # Create a new workbook and get the active worksheet
    workbook = openpyxl.Workbook()
    worksheet = workbook.active

    # Write the header row
    worksheet.append(['Car Brand','Car Name', 'Car Model', 'Car Gear', 'Car Fuel', 'Car Kilometer', 'Result'])

    # Write the data rows
    for obj in queryset:
        worksheet.append([obj.car_brand,obj.car_name, obj.car_model, obj.car_gear, obj.car_fuel, obj.car_kilometer, obj.result])

    # Create the HttpResponse object with Excel content type
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="mymodels_data.xlsx"'

    # Save the workbook to the response
    workbook.save(response)

    return response
