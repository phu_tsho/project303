from django.db import models
# Create your models here.

class MyModels(models.Model):
    car_brand = models.CharField(max_length=100)
    car_name = models.CharField(max_length=100)
    car_model = models.CharField(max_length=100)
    car_gear = models.CharField(max_length=100)
    car_fuel = models.CharField(max_length=100)
    car_kilometer = models.IntegerField()
    result = models.FloatField()

    def __str__(self):
        return self.car_name