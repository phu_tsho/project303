import requests
from bs4 import BeautifulSoup

url = "https://www.carwale.com/used/cars-for-sale/#city=1&pc=1&sc=-1&so=-1&pn=10&lcr=216&ldr=0&lir=0"

response = requests.get(url)
html_content = response.content

soup = BeautifulSoup(html_content, "html.parser")

car_names = soup.find_all("h2", class_="card-detail-block")
for car_name in car_names:
    print(car_name.text.strip())
